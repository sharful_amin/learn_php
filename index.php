<?php


$name = "SEIP COURSE";

$number = 100;

$is_active = true;

echo $name;
echo $number;
echo $is_active;

var_dump($name);
echo gettype($number);
echo gettype($is_active);

//pre-defined variable: $_SERVER. $_POST, $_GET;

/*

    Three types of Data type:

    (i) Scaler Data type
        int, float, string, boolean

    (ii) Compound Data type
        Array (A set of data: [1,2,3,4,5,"pondit", "100", true]),
        Object

    (iii) Special Data type
        resource, null

*/

$number = "1000";
var_dump($number);

$number = false;

echo gettype($number);

$number = "5000";
var_dump((int)$number);

$number = 100;

$number = 300;

echo $number;

/*
    unchangeable variable: const and define()

*/

const PI = 3.1416;
echo PI;

define('USER', 'ROOT USER');
echo USER;


 echo <<<SHARFUL
 I want to learn PHP.
SHARFUL;

    $data = <<<SHARFUL
    <h4> I want to learn PHP.</h4>
SHARFUL;

echo $data;

echo 21.17

?>